# SDWAN-CXTM-Testcases
- Created by Gowtham Tamilselvan
- Cisco CX TTG SRE Team

## Objective
Create CXTM test cases for SDWAN EFT Code Testing

Note:
- This repo contains modified version of custom keywords initially built by OBS SVS SDWAN team.
- https://wwwin-github.cisco.com/SVS-DELIVERY/OBS-SDWAN/tree/master/resources
