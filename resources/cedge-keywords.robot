# OBS SVS SDWAN custom keywords for vEdge devices.
# https://wwwin-github.cisco.com/SVS-DELIVERY/OBS-SDWAN/tree/master/resources

*** Keywords ***

# Pre-test clear commands
Run clear commands
    [Tags]    PRE-TEST
    #run "clear interface statistics"
    #run "clear system statistic"
    run "clear sdwan bfd transitions"
    run "clear ip nat statistics"
    run "clear sdwan app-fwd dpi flow-all"
    #run "clear policy zbfw global-statistics"
    run "clear sdwan notification stream viptela"
    run "no debug all"
    #run "clear ip nat filter"
    #run "clear app dpi all"
    run "clear sdwan policy access-list"
    run "clear sdwan policy app-route-policy"
    run "clear sdwan policy data-policy"
    #run "clear policy zbfw filter-statistics"
    #run "clear policy zbfw global-statistics"
    #run "clear policy zbfw sessions"

Log pre-test cedge show commands
    [Tags]    PRE-TEST
    run "show version"
    run "show clock"
    run "show sdwan system status"
    run "show interface stats"
    run "show interface summary"
    run "show ip interface brief"
    run "show sdwan nat-fwd ip-nat-translation"
    run "show sdwan control local-properties"
    run "show sdwan control connections"
    run "show ntp associations"
    run "show sdwan omp peers"
    run "show sdwan omp summary"
    run "show sdwan omp tlocs"
    run "show sdwan omp tloc-paths"
    run "show sdwan bfd sessions"
    run "show vrrp"
    run "show bgp neighbor"
    run "show bgp summary"
    run "show ospf neighbor"
    run "show ospf ipv4 interface brief"
    run "show ip route summary"
    run "show sdwan ipsec local-sa"
    run "show sdwan tunnel gre-keepalives"
    run "show sdwan ipsec local-sa"
    run "show sdwan ipsec outbound-connections"
    run "show sdwan app-fwd cflowd statistics"
    run "show sdwan app-fwd dpi flows"
    run "show sdwan app-route stats"
    run "show sdwan policy from-vsmart"
    run "show sdwan policy access-list-associations"
    run "show sdwan policy access-list-counters"
    run "show sdwan notification stream viptela"

Log vShell log files before test
    [Tags]    PRE-TEST
    run "vshell"
    run "date"
    run "tail -n 20 /var/log/messages"
    run "tail -n 20 /var/log/vsyslog"
    run "top -b -n 1 > top-before.txt"
    run "cat top-before.txt"
    run "rm top-before.txt"
    run "exit"

Log vShell log files after test
    [Tags]    PRE-TEST
    run "vshell"
    run "date"
    run "tail -n 100 /var/log/messages"
    run "tail -n 100 /var/log/vsyslog"
    run "top -b -n 1 > top-after.txt"
    run "cat top-after.txt"
    run "rm top-after.txt"
    run "exit"

# Step 2 Start Ixia Flows (Check below)


# Step 3 Verify Control Connections and Transport Side 

Verify VPN0 Interfaces Statistics
    [Tags]    VPN0
    ${wan}=  run "show sdwan running-config sdwan"
    ${interface_list}=  extract patterns "GigabitEthernet[0-9]\/[0-9]\/[0-999]"
    Log  ${wan}
    Log  ${interface_list}
    # Show VPN0 Interface Statistics
    FOR   ${interface}  IN   @{interface_list}
          ${show_interface}=  execute "show interface ${interface} stats"
    END
    Log   ${show_interface}

Verify VPN0 Sub-Interfaces Statistics
    [Tags]    VPN0
    ${wan}=  run "show sdwan running-config sdwan"
    ${interface_list}=  extract patterns "GigabitEthernet[0-9]\/[0-9]\/[0-999]\.[0-9]+"
    Log  ${wan}
    Log  ${interface_list}
    # Show VPN0 Interface Statistics
    FOR   ${interface}  IN   @{interface_list}
          ${show_interface}=  execute "show interface ${interface} status"
    END
    Log   ${show_interface}

Verify VPN0 IPSec Statistics
    [Tags]    VPN0
    ${wan}=  run "show running-config vpn 0 interface"
    ${interface_list}=  extract patterns "ipsec[1-2]"
    Log  ${wan}
    Log  ${interface_list}
    # Show VPN0 Interface Statistics
    FOR   ${interface}  IN   @{interface_list}
          ${show_interface}=  execute "show interface statistics ${interface}"
    END
    Log   ${show_interface}

Verify VPN0 Interfaces Status
    [Tags]    VPN0
    ${wan}=  run "show running-config vpn 0 interface"
    ${interface_list}=  extract patterns "ge[0-2]\\/[0-999]"
    Log  ${wan}
    Log  ${interface_list}
    # Show VPN0 Interface Statistics
    FOR   ${interface}  IN   @{interface_list}
          ${show_interface}=  execute "show interface ${interface}"
    END
    Log   ${show_interface}

Verify IP ROUTE VPN 0
    [Tags]    VPN0
    ${show_ip_route}=  run "show ip route"
    output contains "0.0.0.0/0"
    Log  ${show_ip_route}

# Step 4 verify management 512 functionality

Verify mgmt vpn 512 connectivity with IP "${IP}"
    [Tags]    MGMT
    #use device "${JH_1}"
    #Ping MGMT VPN 512 Interface
    ${ping_vpn_512}=  run "ping ${IP} -c 5"
    output matches pattern "5 received, 0% packet loss"
    Log  ${ping_vpn_512}

Verify SSH with TACACS defined user
    [Tags]    MGMT
    ${output}=  execute "show sdwan run username"
    output contains "admin"

Verify SNMP
    [Tags]    MGMT
    [Arguments]    ${DUT_512}
    #use device "${JH_1}"
    #SNMP Walk of DUT
    ${snmp_walk}=  run "snmpwalk -Os -c SNMP-VIEW -v 2c ${DUT_512}"
    output matches pattern "Viptela SNMP agent"
    Log  ${snmp_walk}

Verify NTP functionality
    [Tags]    MGMT
    [Arguments]    ${DUT}
    use device "${DUT}"
    # Check NTP status on DUT
    ${ntp_config}=  run "show sdwan run system"
    output matches pattern "${NTP_SERVER_PRI}"
    Log  ${ntp_config}
    run "show ntp associations"
    output matches pattern "yes"

Verify Syslog configration
    [Tags]    MGMT
    # Check syslog config on DUT
    ${syslog_config}=  run "show log"
    output matches pattern "System logging to host ${SYSLOG_SERVER}"
    Log  ${syslog_config}

Verify Syslog message
    [Tags]    MGMT
    [Arguments]    ${DUT_511}
    #use device "${JH_1}"
    #SNMP Walk of DUT
    ${snmp_walk}=  run "snmpwalk -Os -c SNMP-VIEW -v 2c ${DUT_511}"
    output matches pattern "Viptela SNMP agent"
    Log  ${snmp_walk}

# Step 5 Verify service side routing

Verify OSPF Interface
    [Tags]    OSPF
    [Arguments]    ${DUT_VPN1_IP}
    # Check OSPF Interface
    ${ospf_interface}=  run "show ip ospf interface"
    output matches pattern "${DUT_VPN1_IP}"
    Log  ${ospf_interface}

Verify OSPF Neighbor Adjacency
    [Tags]    OSPF
    [Arguments]    ${ospf_neighbor}
    # Check OSPF Neighbor Adjacency
    ${ospf_adjacency}=  run "show ip ospf neighbor detail"
    output matches pattern "${ospf_neighbor}"
    Log  ${ospf_adjacency}

Verify OSPF Dual Neighbor Adjacency
    [Tags]    OSPF
    # Check OSPF Neighbor Adjacency
    ${output}=  run "show ospf neighbor | display json"
    ${ospf_adjacency}=   Convert String to Json  ${output}
    ${nbrs}=    Get value from json   ${ospf_adjacency}   $..source
    ${nbr_count}=  Get Length  ${nbrs}
    Should be equal  ${nbr_count}    ${2}
    ${nbrs_state}=    Get value from json   ${ospf_adjacency}   $..neighbor-state
    List Should Not Contain Value   ${nbrs_state}   exstart
    Log  ${ospf_adjacency}

Verify OSPF Routes
    [Tags]    OSPF
    [Arguments]    ${ospf_external}
    # Check OSPF Routes
    ${ospf_routes}=  run "show ip ospf rib"
    output matches pattern "${ospf_external}"
    Log  ${ospf_routes}

Verify OSPF Prefixes
    [Tags]    OSPF
    [Arguments]    ${ip_prefixes}
    # Check OSPF Routes
    ${ospf_routes}=  run "show ip ospf database"
    output matches pattern "${ip_prefixes}"
    Log  ${ospf_routes}

Verify IP Route Summary
    [Tags]    OSPF
    [Arguments]    ${ip_prefixes}
    # Check OSPF Routes
    ${route_summary}=  run "show ip route summary"
    output matches pattern "${ip_prefixes}"
    Log  ${route_summary}

Verify OSPF OMP Redistribtion
    [Tags]    OSPF
    # Check OSPF Neighbor Adjacency
    ${ospf_omp}=  run "show run | section ospf"
    output matches pattern "redistribute omp"
    Log  ${ospf_omp}

Verify OSPF database external link-id Metric 20
    [Tags]    OSPF
    [Arguments]    ${ospf_external_prefix}
    # Check OSPF database external link-id
    ${ospf_external}=  run "show ospf database external link-id ${ospf_external_prefix} detail"
    output matches pattern "Metric - 20"
    Log  ${ospf_external}

Verify OSPF database external link-id Metric 100
    [Tags]    OSPF
    [Arguments]    ${ospf_external_prefix}
    # Check OSPF database external link-id
    ${ospf_external}=  run "show ospf database external link-id ${ospf_external_prefix} detail"
    output matches pattern "Metric - 100"
    Log  ${ospf_external}

Verify BGP Summary
    [Tags]    BGP
    # Check BGP Summary
    ${bgp_summary}=  run "show bgp summary"
    output matches pattern "established"
    Log  ${bgp_summary}

Verify BGP Neighbor Adjacency
    [Tags]    BGP
    [Arguments]    ${bgp_neighbor}
    # Check BGP Neighbor Adjacency
    ${bgp_adjacency}=  run "show bgp neighbor detail"
    output matches pattern "${bgp_neighbor}"
    Log  ${bgp_adjacency}

Verify BGP Routes
    [Tags]    BGP
    # Check BGP Routes
    ${ospf_routes}=  run "show bgp routes"
    output matches pattern "0.0.0.0/0"
    Log  ${ospf_routes}

# Step 6 : Verify service side features

Verify dpi flows
    [Tags]    DPI
    # Find current dpi flows
    ${dpi_summary}=  run "show app dpi summary"
    output matches pattern "enable"
    ${dpi_current_count}=  run "show app dpi summary current-flows"
    ${dpi_count}=  extract patterns "\\+?\\d+$"
    Set Global Variable  ${dpi_count}
    Log  ${dpi_summary}

Verify dpi applications
    [Tags]    DPI
    # Find current dpi applications
    ${dpi_applications}=  run "show app dpi applications"
    #output matches pattern "edonkey"
    Log  ${dpi_applications}

Verify ZBFW sessions
    [Tags]    ZBFW
    # Find current dpi applications
    ${zbfw_sessions}=  run "show policy zbfw sessions"
    #output matches pattern ""
    ${zbfw_global_stats}=  run "show policy zbfw global-statistics"
    #output matches pattern ""
    Log  ${zbfw_sessions}
    Log  ${zbfw_global_stats}

Verify NAT
    [Tags]    NAT
    # Find current dpi applications
    ${nat_interfaces}=  run "show ip nat translations"
    #output contains "Total number of translations: 0"
    ${nat_statistics}=  run "show ip nat statistics"
    #output contains "Total active translations: 0 (0 static, 0 dynamic; 0 extended)"
    Log  ${nat_interfaces}
    Log  ${nat_statistics}

Verify application routing
    [Tags]    APP-ROUTE
    [Arguments]    ${REMOTE_SYS_IP}
    # Find current dpi applications
    ${app_route_stats}=  run "show app-route stats remote-system-ip ${REMOTE_SYS_IP}"
    #output matches pattern ""
    Log  ${app_route_stats}

Verify QOS classification
    [Tags]    QOS
    # Find current dpi applications
    ${qos_filter}=  run "show sdwan policy data-policy-filter"
    #output matches pattern ""
    Log  ${qos_filter}

# Post-test clean up

# End of test clean up
#     [Tags]    POST-TEST
#     connect to device "${JH_1}" over "vty"
#     run "export LC_ALL=C"
#     run "export TERM=dumb"
#     run "export LC_ALL\=en_US.UTF-8"
#     run "cd /root/Documents/bps"
#     run "> temp.csv"

# VRRP

Verify device is primary VRRP
    run "show vrrp vpn 1"
    output contains "master"

Verify device is secondary VRRP
    run "show vrrp vpn 1"
    output contains "backup"

Shutdown interface "${int}"
    run "configure terminal"
    run "interface ${int}"
    run "shutdown"
    run "end"

No shutdown interface "${int}"
    run "configure terminal"
    run "interface ${int}"
    run "no shutdown"
    run "end"

# IPSEC Tunnels

Verify ipsec sessions 
    [Tags]    IPSEC
    # Verify IPSEC IKE sessions
    ${output}=  run "show ipsec ike sessions | display json"
    ${ipsec_sesssions}=   Convert String to Json  ${output}
    ${ipsec_tunnels}=    Get value from json   ${ipsec_sesssions}   $..if-name
    ${tunnel_count}=  Get Length  ${ipsec_tunnels}
    Should be equal  ${tunnel_count}    ${2}
    ${tunnel_state}=    Get value from json   ${ipsec_sesssions}   $..state
    List Should Contain Value   ${tunnel_state}   IKE_UP_IPSEC_UP
    Log  ${ipsec_sesssions}

# Reboot Router

Reboot router
    [Arguments]    ${DUT}
    use device "${DUT}"
    run "reboot now"
    sleep  ${SLEEP_30}


Reconnect after router reboot
    [Arguments]    ${DUT}
    connect to device "${DUT}" over "vty"
    run "screen-length 0"

Disconnect from device
    [Arguments]    ${DUT}
    disconnect from device "${DUT}"

# Shutdown IPSec Tunnels

Shutdown tunnel1                                                                                                 
    [Arguments]    ${DUT}   ${int}
    connect to device "${DUT}" over "vty"
    run "configure terminal"
    run "interface ${int}"
    run "shutdown"
    run "end"

Shutdown tunnel2
    [Arguments]    ${DUT}   ${int}
    connect to device "${DUT}" over "vty"
    run "configure terminal"
    run "interface ${int}"
    run "shutdown"
    run "end"

No Shutdown tunnel1
    [Arguments]    ${DUT}   ${int}
    connect to device "${DUT}" over "vty"
    run "configure terminal"
    run "interface ${int}"
    run "no shutdown"
    run "end"

No Shutdown tunnel2
    [Arguments]    ${DUT}   ${int}
    connect to device "${DUT}" over "vty"
    run "configure terminal"
    run "interface ${int}"
    run "no shutdown"
    run "end"

# vManage API

Verify BFD sessions
    [Arguments]    ${DUT_sys_ip}
    ${result}=  sdwan API get object URI "device/bfd/summary?deviceId=${DUT_sys_ip}" from "${VMANAGE}"
    ${result1}=  jq filter   text=${result}   filter=.data[0]."bfd-sessions-up"
    ${result2}=  jq filter   text=${result}   filter=.data[0]."bfd-sessions-total"
    Log To Console    BFD status:
    Log To Console    BFD sessions up: ${result1}
    Log To Console    BFD sessions total: ${result2}
    Should Be True  ${result1}==${result2}

Verify Control Connections
    [Arguments]    ${DUT_sys_ip}
    ${result}=  sdwan API get object URI "device/counters?deviceId=${DUT_sys_ip}" from "${VMANAGE}"
    ${result1}=  jq filter   text=${result}   filter=.data[0]."number-vsmart-control-connections"
    ${result2}=  jq filter   text=${result}   filter=.data[0]."expectedControlConnections"
    Log To Console    OMP status:
    Log To Console    Control connections up: ${result1}
    Log To Console    Expected control connections: ${result2}
    Should Be True  ${result1}==${result2}

Verify Out of Sync Controllers
    [Arguments]    ${DUT_sys_ip}
    ${result}=  sdwan API get object URI "device/controllers/vedge/status" from "${VMANAGE}"
    ${result1}=  jq filter   text=${result}   filter=.data[0]."ControllersOutOfSync"
    ${result2}=  jq filter   text=${result}   filter=.data[0]."expectedControlConnections"
    Log To Console    Controller status:
    Log To Console    Control out of sync: ${result1}
    Log To Console    Controllers up: ${result2}
    Should Be True  ${result1}=="0"

