*** Settings ***
Library  CXTA
Resource  cxta.robot
Resource  ../resources/cedge-keywords.robot
Variables  ../resources/globalvariables.yaml

# Include all the keywords from the opensource libraries from robot framework:
# http://robotframework.org/robotframework/#standard-libraries
Library     String
Library     BuiltIn
Library     Dialogs
Library     DateTime
Library     Collections
Library     OperatingSystem
Library     requests

Suite Setup   Test Setup
#Suite Teardown  Test Cleanup

#Suite Setup     Run Keywords
#...             Test Setup        #load testbed file

Suite Teardown      Run Keywords
...                 disconnect from all devices     #disconnect from all devices

*** Variables ***
${ROBOT_CONTINUE_ON_FAILURE}  True
#${PATH}           ../tmp/output/devices-list.txt

*** Test Cases ***

# Node 2A-1 - pEdge3-ISR4331-1 Test case

Step 1: Verify Status of VPN0 Interfaces
    Verify Control Connections   DUT_sys_ip=${2A1_sys_ip}
    Verify BFD sessions   DUT_sys_ip=${2A1_sys_ip}
    use device "${2A1}"
    Verify VPN0 Sub-Interfaces Statistics
    Verify IP ROUTE VPN 0

Step 2: MGMT VPN 512 functionality
    use device "${JH_1}"
    Verify mgmt vpn 512 connectivity with IP "${2A1_vpn512_ip}"
    Verify SNMP   DUT_512=${2A1_vpn512_ip}
    use device "${2A1}"
    Verify SSH with TACACS defined user
    #Verify NTP functionality   DUT=${cEdge11}
    #Verify Syslog configration

Step 3: Verify service side VPN routing and traffic flow
    use device "${2A1}"
    Verify OSPF Interface   DUT_VPN1_IP=${2A1_vpn1_ip}
    Verify OSPF Neighbor Adjacency   ospf_neighbor=${2A1_ospf_neighbor}
    Verify OSPF Routes   ospf_external=${2A1_ospf_external} 
    Verify OSPF OMP Redistribtion

Step 4: Verify service side features
    use device "${2A1}"
    #Verify dpi flows
    #Verify dpi applications
    #Verify ZBFW sessions
    Verify NAT
    #Verify application routing   REMOTE_SYS_IP=${2A1_sys_ip}
    Verify QOS classification
    #Verify ACL to 5A   site_lan_vlan=${1A_lan_vlan}  site_lan_ip1=${1A_vm_ip1}

#Step 5: Verify Ixia traffic flow statistics
    #sleep  ${SLEEP_60}
    #Check Ixia BP Flow

Step 6: Run post-test logs
    #sleep  ${SLEEP_60}
    use device "${2A1}"
    Log pre-test cedge show commands
    #Log vShell log files after test


*** Keywords ***

Test Setup
    # Tell Robot Framework what testbed file to use.
    Load testbed "resources/testbed.yaml"
    # connect to device "${cnalab@sdwan-centosserver}"

    # Step 1: SSH to device via mgmt VPN 512
    sdwan API login to "${VMANAGE}"
    # DUT1 Device Configuration
    #${DUT1_config}=  sdwan get running-config for "${2A1}" via "${VMANAGE}"
    #Log  ${DUT1_config}
    connect to device "${2A1}" over "vty"
    run "screen-length 0"
    Run clear commands
    Log pre-test cedge show commands
    #Log vShell log files before test

    # Step 2 Start Ixia Flows (NA for our setup)

