*** Settings ***
Library  CXTA
Resource  cxta.robot

# Include all the keywords from the opensource libraries from robot framework:
# http://robotframework.org/robotframework/#standard-libraries
Library     String
Library     BuiltIn
Library     Dialogs
Library     DateTime
Library     Collections
Library     OperatingSystem
Library     requests

Suite Setup     Run Keywords
...             load testbed        #load testbed file

Suite Teardown      Run Keywords
...                 disconnect from all devices     #disconnect from all devices

*** Variables ***
${ROBOT_CONTINUE_ON_FAILURE}  True
${PATH}           ../tmp/output/devices-list.txt

*** Test Cases ***
Test Setup
    # Connect to vManage101
    sdwan API login to "vManage101"

#*** Test Cases ***
List Devices
    ${DEVICE_LIST}=  sdwan API get devices from "vManage101"
    Log  ${DEVICE_LIST}
    Create File    ${PATH}    # Text file created at current directory
    ${DEVICE_STRING}=  Convert To String  ${DEVICE_LIST}
    #Write Output to a File
    Append To File  devices-list.txt  ${DEVICE_STRING}



    
